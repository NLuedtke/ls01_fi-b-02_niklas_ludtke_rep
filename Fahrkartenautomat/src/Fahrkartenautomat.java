﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {    	
    	boolean powerStatus = true;
    	do{
    	
    		Scanner tastatur = new Scanner(System.in);
    		double zuZahlenderBetrag; 
    		double eingezahlterGesamtbetrag;
    		//Fahrkartenbestellung
    		// -------------------
    		zuZahlenderBetrag = fahrkartenBestellungErfassen(tastatur);

    		// Geldeinwurf
    		// -----------
    		eingezahlterGesamtbetrag = fahrkartenBezahlen(tastatur, zuZahlenderBetrag);
       

    		// Fahrscheinausgabe
    		// -----------------
    		fahrkartenAusgeben();
       

    		// Rückgeldberechnung und -Ausgabe
    		// -------------------------------
    		rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
       

    		System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    		//Power-Check
    		//-------------------------------
    		powerStatus = powerCheck(tastatur, powerStatus);
    		
    	}while(powerStatus == true);
    }


	static double fahrkartenBestellungErfassen(Scanner tastatur) {
		
		double[] ticketPreise = {2.90,3.30,3.60,1.90,8.60,9.00,9.60,23.50,24.30,24.90};
		int[] anzahlTicket = {0,0,0,0,0,0,0,0,0,0};
		String[] ticketInformation = {"Einzelfahrschein AB","Einzelfahrschein BC","Einzelfahrschein ABC",
				"Kurzstrecke","Tageskarte AB","Tageskarte BC","Tageskarte ABC",
				"Kleingruppen-Tageskarte AB","Kleingruppe-Tageskarte BC","Kleingruppe-Tageskarte ABC"};
		int[] auswahlnummer = {1,2,3,4,5,6,7,8,9,10};
		double zuZahlenderBetrag=0;
		int eingabeTicket;
		boolean mehrTickets=true;
		char weiteresTicket;
			System.out.println("Mögliche Tickets:");
			System.out.printf("%10s%20s%30s%n","Auswahlnummer","Bezeichner","Preis in Euro");
		for(int b=0;b<ticketInformation.length;b++) {
			System.out.printf("%4s%5s%30s%15s", auswahlnummer[b] ," " , ticketInformation[b], "");
			System.out.printf("%s%n", ticketPreise[b]);
		}
		do {
		
			System.out.print("Welches Ticket wollen sie haben: ");
	       eingabeTicket = tastatur.nextInt();
	       
	       anzahlTicket[eingabeTicket-1] = anzahlTicket[eingabeTicket-1]+1;
	      
	       System.out.print("Weiteres Tickets? (y/n): ");
			weiteresTicket = tastatur.next().charAt(0);
			
	   		if(weiteresTicket == 'y' || weiteresTicket == 'Y') {
	   			mehrTickets = true;
	   		}
	   		else {
	   			mehrTickets = false;
	   		}
		}while(mehrTickets == true);
		
		for(int a=0;a<10;a++) {
			zuZahlenderBetrag = zuZahlenderBetrag+(anzahlTicket[a]*ticketPreise[a]);
		}
	       
		return zuZahlenderBetrag;
	}


	static double fahrkartenBezahlen(Scanner tastatur, double zuZahlenderBetrag) {
		
		double eingeworfeneMünze=0;
		double eingezahlterGesamtbetrag;
		
		eingezahlterGesamtbetrag = 0.0;
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.printf("%s%.2f%s%n","Noch zu zahlen: " , + (zuZahlenderBetrag - eingezahlterGesamtbetrag), " Euro");
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	   eingeworfeneMünze = tastatur.nextDouble();
	    	   if(eingeworfeneMünze == 0.05 || eingeworfeneMünze== 0.1 || eingeworfeneMünze == 0.2 || eingeworfeneMünze == 0.5 || eingeworfeneMünze == 1 || eingeworfeneMünze == 2) {
		           eingezahlterGesamtbetrag += eingeworfeneMünze;
	    	   }
	    	   else {
	    		   System.out.println("Keine gültige Münze, bitte werfen sie eine andere ein.");
	    	   }

	       }
		return eingezahlterGesamtbetrag;
	}
	
	
	static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
		
	}



	static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		
		double rückgabebetrag;
		
		rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       rückgabebetrag = Math.round(100.0*rückgabebetrag)/100.0;			//hilft leider nur in manchen Fällen
	       if(rückgabebetrag > 0.0)
	       {
	    	   System.out.printf("%s%.2f%s","Der Rückgabebetrag in Höhe von " , rückgabebetrag, " EURO ");
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
		          rückgabebetrag -= 2.0;
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
		          rückgabebetrag -= 1.0;
	           }
	           while(rückgabebetrag >= 0.49) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.18) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag >= 0.08) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.04)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          rückgabebetrag -= 0.05;
	           }
	       }
		
	}
	
	
	static boolean powerCheck(Scanner tastatur, boolean powerStatus) {
    	char power='y';
		System.out.print("Ausschalten? (y/n): ");
		power = tastatur.next().charAt(0);
		
   		if(power == 'y' || power == 'Y') {
   			powerStatus = false;
   		}
   		else {
   			powerStatus = true;
   		}
		return powerStatus;
	}

}