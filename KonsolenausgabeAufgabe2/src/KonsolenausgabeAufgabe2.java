
public class KonsolenausgabeAufgabe2 {

	public static void main(String[] args) {
		
	/*	String nullF = "";
		String einsF = "1";
		String zweiF = "1 * 2";
		String dreiF = "1 * 2 * 3";
		String vierF = "1 * 2 * 3 * 4";
		String fünfF = "1 * 2 * 3 * 4 * 5";
		
		System.out.printf("%-5s","1!","%s","="); */
		
		System.out.printf("%-5s%s%-19s%s%4d%n", "0!","=","","=",1);
		System.out.printf("%-5s%s%-19s%s%4d%n", "1!","=","1","=",1);
		System.out.printf("%-5s%s%-19s%s%4d%n", "2!","=","1 * 2","=",(1*2));
		System.out.printf("%-5s%s%-19s%s%4d%n", "3!","=","1 * 2 * 3","=",(1*2*3));
		System.out.printf("%-5s%s%-19s%s%4d%n", "4!","=","1 * 2 * 3 * 4","=",(1*2*3*4));
		System.out.printf("%-5s%s%-19s%s%4d%n", "5!","=","1 * 2 * 3 * 4 * 5","=",(1*2*3*4*5));
	
	}

}
