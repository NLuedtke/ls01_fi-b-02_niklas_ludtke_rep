import java.io.InputStream;
import java.util.Scanner;
import java.io.Console;

public class Benutzerverwaltung {

    public static void main(String[] args) {

        BenutzerverwaltungV10.start();

    }
}

class BenutzerverwaltungV10{
    public static void start(){
        BenutzerListe benutzerListe = new BenutzerListe();

        benutzerListe.insert(new Benutzer("Paula", "paula"));
        benutzerListe.insert(new Benutzer("Adam37", "adam37"));
        benutzerListe.insert(new Benutzer("Darko", "darko"));
        
        Scanner tastatur = new Scanner(System.in);
        int menuAuswahl = 0;
        boolean richtigeDaten = false;
        String nutzername = "testdaten";
        String nutzerpasswort;
        String kontrollpasswort;
        boolean registriert = false;
        boolean eindeutigerNutzer = false;
        
        
        System.out.println("(1) Anmelden : ");
        System.out.println("(2) Registrieren : ");
        System.out.print("Auswahl (1)(2): ");
        menuAuswahl = tastatur.nextInt();
        
        
        
        if (menuAuswahl == 1) {
        	for (int i=0;i<3 && richtigeDaten == false;i++) {
        		System.out.print("Benutzername: ");
        		nutzername = tastatur.next();
        		System.out.print("Passwort: ");
        		nutzerpasswort = tastatur.next();
        			if (benutzerListe.select(nutzername).equals(nutzername +" "+ nutzerpasswort)) {
        				richtigeDaten = true;
        				System.out.println("Sie sind angemeldet!");
        			}
        			else {
        				System.out.println("Falscher Name oder falsches Passwort.");
        				System.out.println("Bitte versuchen sie es erneut.");
        				System.out.println("Weitere versuche:"+ (2-i));
        			}
        	}
        }
        else {
        	while (registriert == false) {
        		while (eindeutigerNutzer == false) {
        			System.out.print("Eindeutigen Nutzername vergeben: ");
        			nutzername = tastatur.next();
        				if (benutzerListe.select(nutzername) != "") {
        					System.out.println("Nutzername vergeben, bitte erneut versuchen!");
        				}
        				else {
        					eindeutigerNutzer = true;
        				}
        			
        		}
        	System.out.print("Passwort vergeben: ");
    		nutzerpasswort = tastatur.next();
    		System.out.print("Passwort erneut eingeben: ");
    		kontrollpasswort = tastatur.next();
    			if (nutzerpasswort.equals(kontrollpasswort)) {
    				System.out.println("Passwort wurde gesetzt.");
    				registriert = true;
    			}
    		benutzerListe.insert(new Benutzer(nutzername, nutzerpasswort));
        	}
        }
    }
}

class BenutzerListe{
    private Benutzer first;
    private Benutzer last;
    public BenutzerListe(){
        first = last = null;
    }
    public void insert(Benutzer b){
        // Sicherheitshalber setzen wir
        // den Nachfolger auf null:
        b.setNext(null);
        if(first == null){
            first = last = b;
        }
        else{
            last.setNext(b);
            last = b;
        }
    }
    public String select(){
        String s = "";
        Benutzer b = first;
        while(b != null){
            s += b.toString() + '\n';
            b = b.getNext();
        }
        return s;
    }
    public String select(String name){
        Benutzer b = first;
        while(b != null){
            if(b.hasName(name)){
                return b.toString();
            }
            b = b.getNext();
        }
        return "";
    }

    public boolean delete(String name){
        // ...
        return true;
    }
}

class Benutzer{
    private String name;
    private String passwort;

    private Benutzer next;

    public Benutzer(String name, String pw){
        this.name = name;
        this.passwort = pw;

        this.next = null;
    }

    public boolean hasName(String name){
        return name.equals(this.name);
    }

    public String toString(){
        String s = "";
        s += name + " ";
        s += passwort;
        return s;
    }

    public Benutzer getNext(){
        return next;
    }

    public void setNext(Benutzer b){
        next = b;
    }
}

