import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	boolean powerStatus = true;
    	char power='y';
    	
    	do{
    	
    		Scanner tastatur = new Scanner(System.in);
    		double zuZahlenderBetrag; 
    		double eingezahlterGesamtbetrag; 
    		//Fahrkartenbestellung
    		// -------------------
    		zuZahlenderBetrag = fahrkartenBestellungErfassen(tastatur);

    		// Geldeinwurf
    		// -----------
    		eingezahlterGesamtbetrag = fahrkartenBezahlen(tastatur, zuZahlenderBetrag);
       

    		// Fahrscheinausgabe
    		// -----------------
    		fahrkartenAusgeben();
       

    		// Rückgeldberechnung und -Ausgabe
    		// -------------------------------
    		rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
       

    		System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    		//Power-Check
    		//-------------------------------
    		powerStatus = powerCheck(tastatur, powerStatus, power);
    		
    	}while(powerStatus == true);
    }


	static boolean powerCheck(Scanner tastatur, boolean powerStatus, char power) {
		System.out.println("Ausschalten? (y/n): ");
		power = tastatur.next().charAt(0);
		
   		if(power == 'y' || power == 'Y') {
   			powerStatus = false;
   		}
   		else {
   			powerStatus = true;
   		}
		return powerStatus;
	}


	static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
		
	}


	static double fahrkartenBestellungErfassen(Scanner tastatur) {
		
		final double preisTicketEinzel = 2.90;
		final double preisTicketTages = 8.60;
		final double preisTicketGruppe = 23.50;
		double zuZahlenderBetrag=0;
		int eingabeTicket;
		int anzahlTicketEinzel=0;
		int anzahlTicketTages=0;
		int anzahlTicketGruppe=0;
		boolean mehrTickets=true;
		char weiteresTicket;
		
		System.out.printf("%s%n%s%n%s%n%s%n","Mögliche Tickets:","Einzelfahrschein Regeltarif AB [2,90 EUR] (1)", "Tageskarte Regeltarif AB [8,60 EUR]", "Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
		do {
			System.out.print("Welches Ticket wollen sie haben (1/2/3): ");
	       eingabeTicket = tastatur.nextInt();
	       switch (eingabeTicket) {
	       case 1:
	    	   anzahlTicketEinzel = anzahlTicketEinzel+1;
	    	   break;
	       case 2:
	    	   anzahlTicketTages = anzahlTicketTages+1;
	    	   break;
	       case 3:
	    	   anzahlTicketGruppe = anzahlTicketGruppe+1; 
	       }
	       System.out.print("Weiteres Tickets? (y/n): ");
			weiteresTicket = tastatur.next().charAt(0);
			
	   		if(weiteresTicket == 'y' || weiteresTicket == 'Y') {
	   			mehrTickets = true;
	   		}
	   		else {
	   			mehrTickets = false;
	   		}
		}while(mehrTickets == true);
			
		zuZahlenderBetrag = (anzahlTicketEinzel*preisTicketEinzel)+(anzahlTicketTages*preisTicketTages)+(anzahlTicketGruppe*preisTicketGruppe);
	       
		return zuZahlenderBetrag;
	}


	static double fahrkartenBezahlen(Scanner tastatur, double zuZahlenderBetrag) {
		
		double eingeworfeneMünze;
		double eingezahlterGesamtbetrag;
		
		eingezahlterGesamtbetrag = 0.0;
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.printf("%s%.2f%s%n","Noch zu zahlen: " , + (zuZahlenderBetrag - eingezahlterGesamtbetrag), " Euro");
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	   eingeworfeneMünze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneMünze;
	       }
		return eingezahlterGesamtbetrag;
	}

	static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		
		double rückgabebetrag;
		
		rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       rückgabebetrag = Math.round(100.0*rückgabebetrag)/100.0;			//hilft leider nur in manchen Fällen
	       if(rückgabebetrag > 0.0)
	       {
	    	   System.out.printf("%s%.2f%s","Der Rückgabebetrag in Höhe von " , rückgabebetrag, " EURO ");
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
		          rückgabebetrag -= 2.0;
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
		          rückgabebetrag -= 1.0;
	           }
	           while(rückgabebetrag >= 0.49) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.18) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag >= 0.08) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.04)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          rückgabebetrag -= 0.05;
	           }
	       }
		
	}
}