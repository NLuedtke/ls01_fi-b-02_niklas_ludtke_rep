import java.util.Scanner;

public class Rabattsystem {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner (System.in);
		double bestellwert;
		double rabattprozent=10;
		final double mehrwertsteuer=19.00;
		double rabattpreis=0;
		double zwischenpreis;
		double endpreis= 0;
		
		System.out.println("Bestellwert: ");
		bestellwert = tastatur.nextDouble();
		if(bestellwert <= 100 && bestellwert >= 0) {
			rabattprozent = 10;
			rabattpreis = bestellwert*(rabattprozent/100);
			zwischenpreis = bestellwert-rabattpreis;
			endpreis = zwischenpreis + (zwischenpreis*(mehrwertsteuer/100));
		}else if(bestellwert > 100 && bestellwert <= 500) {
			rabattprozent = 15;
			rabattpreis = bestellwert*(rabattprozent/100);
			zwischenpreis = bestellwert-rabattpreis;
			endpreis = zwischenpreis + (zwischenpreis*(mehrwertsteuer/100));
		}else if(bestellwert > 500) {
			rabattprozent = 20;
			rabattpreis = bestellwert*(rabattprozent/100);
			zwischenpreis = bestellwert-rabattpreis;
			endpreis = zwischenpreis + (zwischenpreis*(mehrwertsteuer/100));
		} else {
			System.out.println("Ihre Eingabe ist ungültig, bitte starten sie erneut.");
		}
		System.out.print("Ihr Preis inklusive Rabatt und Mehrwertsteuer beträgt:" + endpreis);
	}

}
