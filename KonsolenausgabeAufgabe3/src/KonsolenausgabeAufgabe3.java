
public class KonsolenausgabeAufgabe3 {

	public static void main(String[] args) {

		System.out.printf("%-15s%s%15s%n", "Fahrenheit", "|", "Celsius");
		System.out.printf("%s%n", "-------------------------------");
		System.out.printf("%+-15d%s%+15.2f%n",-20, "|",-28.88889);
		System.out.printf("%+-15d%s%+15.2f%n",-10, "|",-23.33333);
		System.out.printf("%+-15d%s%+15.2f%n",0, "|",-17.77778);
		System.out.printf("%+-15d%s%15.2f%n",20, "|",-6.66667);
		System.out.printf("%+-15d%s%15.2f%n",30, "|",-1.11111);

	}

}
